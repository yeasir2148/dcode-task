<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateTask;
use App\Http\Requests\CreateTask;

class TasksController extends Controller
{
   public function __construct()
   {
      $this->middleware('auth');
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
      $tasks = Task::where('user_id',auth()->user()->id)->get();
      if (request()->ajax() || App()->runningUnitTests()) {
         return response()->json($tasks);
      }
      return view('tasks', compact('tasks'));
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      return view('task-create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(CreateTask $request)
   {
      $response = [];
      $validatedAttr = $request->validated();

      $newTask = Task::create(
         array_merge($validatedAttr, ['complete' => false, 'user_id' => $request->user()->id])
      );

      if ($newTask->wasRecentlyCreated !== true) {
         $response['success'] = false;
         $response['message'] = 'Cannot create task';
      } else {
         $response['success'] = true;
         $response['data'] = $newTask;
      }

      return json_encode($response);
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Task  $task
    * @return \Illuminate\Http\Response
    */
   public function show(Task $task)
   {
        //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Task  $task
    * @return \Illuminate\Http\Response
    */
   public function edit(Task $task)
   {
        //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Task  $task
    * @return \Illuminate\Http\Response
    */
   public function update(UpdateTask $request, Task $task)
   {
      $validatedAttr = $request->validated();
      if($task->user_id == $request->user()->id) {
         $task->update($validatedAttr);
         $task->refresh();
         if (request()->ajax() || App()->runningUnitTests()) {
            return response()->json($task);
         }
      } else {
         return response()->json(['error' => 'Not authorized.'], 403);
      }      
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Task  $task
    * @return \Illuminate\Http\Response
    */
   public function destroy(Task $task)
   {
      if($task->user_id == $request->user()->id) {
         $success = $task->delete();
         return response()->json(['success' => $success]);
      } else {
         return response()->json(['error' => 'Not authorized.'], 403);
      }
   }
}
