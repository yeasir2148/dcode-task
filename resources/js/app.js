/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import DatePickerComponent from './components/DatePickerComponent.vue';
import 'jquery-ui/ui/widgets/datepicker.js';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const httpConfig = {
   getAll: {
      method: "get",
      url: "/tasks",
      responseType: "json"
   },
   create: {
      method: "post",
      url: "/tasks/create",
      responseType: "json"
   },
   put: {
      method: "put",
      url: "/tasks/update/{task_id}",
      data: {},
      responseType: "json"
   },
   delete: {
      method: "delete",
      url: "/tasks/delete/{task_id}",
      responseType: "json"
   },
};
const app = new Vue({
   el: '#app',
   components: { DatePickerComponent },
   data() {
      return {
         tasks: null,
         form: {
            successMsg: null,
            errorMsg: null,
            deleteMsg: null,
            title: null,
            description: null,
            dueDate: null,
            complete: null,
            updateTaskId: null
         },
         showUpdateModal: false,
         errors: {
            title: null,
            description: null,
            due_by: null
         },
         serverResponseData: {},
      }
   },
   computed: {
      postData: function() {
         return {
            title: this.form.title,
            description: this.form.description,
            due_by: this.form.dueDate,
            complete: this.form.complete
         };
      },
      isFormValid: function() {
         return this.form.title && this.form.description && this.form.dueDate;
      }
   },
   mounted: function() {
      this.fetchTasks();
   },
   methods: {
      fetchTasks: function() {
         axios(httpConfig.getAll)
         .then(({ data }) => {
            if(data !== null && data !== 'undefined') {
               this.tasks = data;
            }
         });
      },
      createTask: function() {
         httpConfig.create.data = this.postData;
         var vm = this;

         axios(httpConfig.create)
            .then((response) => {
               this.serverResponseData = response.data;
               if(this.serverResponseData.success === true) {
                  this.tasks.push(this.serverResponseData.data);
                  this.form.successMsg = 'Task created successfully';
               }
               setTimeout(() => this.resetForm(), 1000);
            })
            .catch(error => {
               let errors = error.response.data.errors;
               if(errors) {
                  for(error in errors) {
                     this.errors[error] = errors[error][0];
                  }
               }
            })
            .finally(() => {
            });
      },
      deleteTask: function(taskId) {
         axios.delete(httpConfig.delete.url.replace('{task_id}', taskId))
         .then( response => {
            this.serverResponseData = response.data;
            if(response.data.success === true) {
               this.form.deleteMsg = "Task removed successfully";
               this.tasks = this.tasks.filter(task => {
                  return task.id !== taskId;
               });
            }
         })
         .catch(errorResponse => {
            this.form.deleteMsg = errorResponse.message;
         })
         .finally(() => {
            httpConfig.delete.url = '/tasks/delete/{task_id}';
            setTimeout(() => { this.form.deleteMsg = null }, 1000);
         });
      },
      prepareUpdateModal: function(taskId) {
         this.showUpdateModal = true;
         this.form.updateTaskId = taskId;
         let targetTaskIndex = this.tasks.findIndex(task => {
            return task.id === taskId;
         });

         this.form.title = this.tasks[targetTaskIndex].title;
         this.form.description = this.tasks[targetTaskIndex].description;
         this.form.dueDate = this.tasks[targetTaskIndex].due_by;
         this.form.complete = this.tasks[targetTaskIndex].complete;

         $('#editTaskModal').modal({
            backdrop: 'static'
         });
      },
      updateTask: function(taskId) {
         httpConfig.put.url = httpConfig.put.url.replace('{task_id}', taskId);
         httpConfig.put.data = this.postData;
         axios(httpConfig.put)
         .then((response) => {
            // console.log(response);
            if(response.data) {
               let targetTaskIndex = this.tasks.findIndex(task => {
                  return task.id === taskId;
               });
               if(targetTaskIndex !== false) {
                  this.tasks.splice(targetTaskIndex, 1, response.data);
               }
            }
         })
         .finally(() => {
            httpConfig.put.url = "/tasks/update/{task_id}";
            this.hideUpdateModal();
         });
      },
      hideUpdateModal: function() {
         this.resetForm();
         $('#editTaskModal').modal('hide');
         this.showUpdateModal = false;
      },
      resetForm: function() {
         for (var key in this.form) {
            this.form[key] = null;
         }
         for (var error in this.errors) {
            this.errors[error] = null;
         }
      }
   },
});
