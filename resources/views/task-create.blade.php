      <div class="tag is-primary is-large">Create Task</div>
      <br/>
      <br/>
      <div class="alert alert-success" v-if="form.successMsg">@{{form.successMsg}}</div>
      <div class="alert alert-danger" v-if="form.errorMsg">@{{form.errorMsg}}</div>
      <div class="form">
         <form id="create_task" @submit.prevent="createTask">
            <div class="field is-horizontal">
               <div class="field-label is-normal">
                  <label for="task_title" class="label">Title<sup>*</sup></label>
               </div>
               <div class="field-body">
                  <div class="field">
                     <div class="control">
                        <input type="text"
                           class="input" name="title" id="task_title"
                           v-model="form.title" required>
                           <span v-if="errors.title" class="alert-danger">@{{errors.title}}</span>
                     </div>
                  </div>
               </div>
            </div>

            <br>
            <!-- quantity -->
            <div class="field is-horizontal">
               <div class="field-label is-normal">
                  <label for="description" class="label">Description<sup>*</sup></label>
               </div>
               <div class="field-body">
                  <div class="field">
                     <div class="control">
                        <textarea name="description" id="description" rows="5"
                           class="textarea" v-model="form.description" required></textarea>
                           <span v-if="errors.description" class="alert-danger">@{{errors.description}}</span>
                     </div>
                  </div>
               </div>
            </div>

            <br>

            <div class="field is-horizontal">
               <div class="field-label is-normal">
                  <label for="due_date" class="label">Due date <sup>*</sup></label>
               </div>
               <div class="field-body">
                  <div class="field">
                     <div class="control">
                        <date-picker-component id="due_date" v-model="form.dueDate"></date-picker-component>
                        <br>
                        <span v-if="errors.due_by" class="alert-danger">@{{errors.due_by}}</span>
                     </div>
                  </div>
               </div>
            </div>

            <div class="field is-horizontal">
               <div class="field-label"></div>
               <div class="field-body">
                  <div class="field">
                     <div class="control">
                        <button
                           class="button is-link"
                           type="submit"
                           :disabled="!isFormValid">
                              Create
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>

