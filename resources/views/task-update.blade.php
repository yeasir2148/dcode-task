<div class="modal edit-task-modal" tabindex="-1" role="dialog" id="editTaskModal">
   <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title has-text-weight-semibold">Update Task</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="hideUpdateModal">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="form">
               <form id="create_task" @submit.prevent="createTask" v-cloak>
                  <div class="field is-horizontal">
                     <div class="field-label is-normal">
                        <label for="task_title" class="label">Title<sup>*</sup></label>
                     </div>
                     <div class="field-body">
                        <div class="field">
                           <div class="control">
                              <input type="text"
                                 class="input" name="title" id="task_title"
                                 v-model="form.title" required>
                           </div>
                        </div>
                     </div>
                  </div>

                  <br>
                  <!-- quantity -->
                  <div class="field is-horizontal">
                     <div class="field-label is-normal">
                        <label for="description" class="label">Description<sup>*</sup></label>
                     </div>
                     <div class="field-body">
                        <div class="field">
                           <div class="control">
                              <textarea name="description" id="description" rows="5"
                                 class="textarea" v-model="form.description" required></textarea>
                           </div>
                        </div>
                     </div>
                  </div>

                  <br>

                  <div class="field is-horizontal">
                     <div class="field-label is-normal">
                        <label for="due_date" class="label">Due date <sup>*</sup></label>
                     </div>
                     <div class="field-body">
                        <div class="field">
                           <div class="control">
                              <date-picker-component id="due_date" v-model="form.dueDate"></date-picker-component>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="field is-horizontal">
                     <div class="field-label is-normal">
                        <label class="label">Complete</label>
                     </div>
                     <div class="field-body">
                        <div class="field">
                           <div class="control">
                              <label class="radio">
                                 <input type="radio" name="due_by" value="1" :checked="form.complete" v-model="form.complete">
                                 Yes
                              </label>
                              <label class="radio">
                                 <input type="radio" name="due_by" value="0" :checked="!form.complete" v-model="form.complete">
                                 No
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" @click.prevent="updateTask(form.updateTaskId)">Update</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="hideUpdateModal">Cancel</button>
         </div>
      </div>
   </div>
</div>