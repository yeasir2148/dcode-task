@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div class="columns is-centered">
      <div class="column is-three-fifths" style="text-align:center">
         <span v-if="form.deleteMsg" class="alert " :class="{'alert-success': serverResponseData.success, 'alert-danger': !serverResponseData.success}">@{{form.deleteMsg}}</span>
         <h2 class="title is-2" style="text-align: center">Your Task List</h2>
         <table class="table is-bordered is-striped is-narrow is-hoverable" v-cloak>
            <thead>
               <tr>
                  <th class="has-text-centered">Title</th>
                  <th class="has-text-centered">Description</th>
                  <th class="has-text-centered">Complete</th>
                  <th class="has-text-centered">Due By</th>
                  @auth
                  <th class="has-text-centered">Action</th>
                  @endauth
               </tr>
            </thead>

            <tbody>
               <tr v-for="task in tasks" :key="task.id">
                  <td class="has-text-centered task-title">@{{ task.title }}</td>
                  <td class="has-text-centered">@{{ task.description && task.description.length > 30 ? task.description.substr(0, 30) + "..." : task.description }}</td>
                  <td class="has-text-centered " :class="{complete: task.complete, incomplete: !task.complete}">@{{ task.complete ? 'Y' : 'N' }}</td>
                  <td class="has-text-centered">@{{ task.due_by }}</td>
                  @auth
                  <td class="has-text-centered">
                     <button title="delete task" @click.prevent="deleteTask(task.id)"><i class="fas fa-trash"></i></button>
                     <button title="edit task" @click.prevent="prepareUpdateModal(task.id)"><i class="fas fa-pen-square"></i></button>
                  </td>
                  @endauth
               </tr>
            </tbody>
         </table>
      </div>

      <div class="column is-two-fifths" style="text-align: center" v-show="!showUpdateModal">
         @auth
            @includeIf('task-create')
         @endauth
      </div>

      @auth
         @include('task-update')
      @endauth
   </div>

</div>
@endsection
