<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use App\User;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
   $user = factory(User::class)->create();
   return [
      'title' => $faker->sentence(5),
      'description' => $faker->sentence,
      'due_by' => $faker->date(),
      'complete' => $faker->boolean(),
      'user_id' => $user->id
   ];
});
